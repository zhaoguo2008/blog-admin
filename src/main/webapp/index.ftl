<#include "/common/_layout.ftl"/>
<@layout>

<div class="container">
    <!--the content-->
    <div class="ui grid">
        <!--the vertical menu-->
        <div class="four wide column">
            <div class="verticalMenu">
                <div class="ui vertical pointing menu fluid">
                    <a class="item" href="./deploy.html">
                        <i class="rocket icon"></i> 部署新产品
                    </a>
                    <a class="active teal item" href="./product_list.html">
                        <i class="cloud icon"></i> 我的产品
                    </a>
                    <a class="item" href="#">
                        <i class="bar chart icon"></i> 产品统计
                    </a>
                </div>
            </div>
        </div>
        <!--the Devicelist-->
        <div class="twelve wide column">
            <div class="pageHeader">
                <div class="segment">
                    <h3 class="ui dividing header">
                        <i class="large cloud icon"></i>
                        <div class="content">
                            我的产品
                            <div class="sub header">生成的产品都在这里</div>
                        </div>
                    </h3>
                </div>
            </div>
            <!--the products content-->
            <div class="ui device two column middle aligned vertical grid segment">
                <div class="column verborder">
                    <div class="ui info segment">
                        <h5 class="ui header">
                            产品01 <span class="ui type label" title="平台公开">平台公开</span>
                            <div class="sub header">这是一个测试中，还未发布的产品。</div>
                        </h5>
                        <p>模板设备： <a class="externalUrl" href="./device.html" title="模板设备">设备01</a></p>
                        <p>已生成： <span class="stress">3</span> 批次</p>
                        <p>已激活： <span class="stress">23 / 50</span>，46.0%</p>
                    </div>
                </div>
                <div class="center aligned column">
                    <div class="ui buttons">
                        <a class="ui tiny blue button" href="./product.html" title="管理产品"><i class="setting icon"></i>管 理</a>
                        <a class="ui tiny basic button"><i class="trash icon" title="删除产品"></i>删 除</a>
                    </div>
                </div>
            </div>
            <!--another products content-->
            <div class="ui device two column middle aligned vertical grid segment">
                <div class="column verborder">
                    <div class="ui info segment">
                        <h5 class="ui header">
                            产品02 <span class="ui type label" title="不公开">不公开</span>
                            <div class="sub header">这是一个测试中，还未发布的产品。</div>
                        </h5>
                        <p>模板设备： <a class="externalUrl" href="./device.html" title="模板设备">设备023</a></p>
                        <p>已生成： <span class="stress">3</span> 批次</p>
                        <p>已激活： <span class="stress">23 / 50</span>，46.0%</p>
                    </div>
                </div>
                <div class="center aligned column">
                    <div class="ui buttons">
                        <a class="ui tiny blue button" href="./product.html" title="管理产品"><i class="setting icon"></i>管 理</a>
                        <a class="ui tiny basic button"><i class="trash icon" title="删除产品"></i>删 除</a>
                    </div>
                </div>
            </div>
            <!--another products content-->
            <div class="ui device two column middle aligned vertical grid segment">
                <div class="column verborder">
                    <div class="ui info segment">
                        <h5 class="ui header">
                            产品03 <span class="ui type label" title="开发商公开">开发商公开</span>
                            <div class="sub header">这是一个测试中，还未发布的产品。</div>
                        </h5>
                        <p>模板设备： <a class="externalUrl" href="./device.html" title="模板设备">温室自动报警</a></p>
                        <p>已生成： <span class="stress">3</span> 批次</p>
                        <p>已激活： <span class="stress">23 / 50</span>，46.0%</p>
                    </div>
                </div>
                <div class="center aligned column">
                    <div class="ui buttons">
                        <a class="ui tiny blue button" href="./product.html" title="管理产品"><i class="setting icon"></i>管 理</a>
                        <a class="ui tiny basic button"><i class="trash icon" title="删除产品"></i>删 除</a>
                    </div>
                </div>
            </div>
            <!--the pagination-->
            <#include "/common/_paginate.ftl" />
            <@paginate currentPage=2 totalPage=10 actionUrl="${CONTEXT_PATH}/post/" />
        </div>
        <!--Pagination-->
    </div>
</div>

</@layout>

<@scripts>


</@scripts>