<#macro layout>
    <!doctype html>
    <html lang="zh-CN">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <title>我的产品 | 用户中心</title>
        <link type="text/css" rel="stylesheet" href="${CONTEXT_PATH}/resource/css/framework.css" />
        <link type="text/css" rel="stylesheet" href="${CONTEXT_PATH}/resource/css/main.css" />
    </head>
    <body>
    <div class="page">
        <!--header begin-->
        <header>
            <div class="bigcontainer">
                <div id="logo">
                    <a href="./">用户中心</a>
                </div>
                <div class="user">
                    <div class="ui inline labeled icon top right pointing dropdown">
                        <img class="ui avatar image" src="${CONTEXT_PATH}/resource/images/avatar-default.gif">
                        欢迎，$用户名
                        <i class="dropdown icon"></i>
                        <div class="menu">
                            <a class="item" href="http://www.rechengit.com"><i class="reply mail icon"></i>返回首页</a>
                            <a class="item" href="/user/logout"><i class="sign out icon"></i>注销登录</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- the main menu-->
        <div class="ui teal inverted menu fluid">
            <div class="bigcontainer">
                <div class="right menu">
                    <a class="item" href="./index.html"><i class="home icon"></i>管理首页</a>
                    <a class="active item" href="./devices_list.html"><i class="sitemap icon"></i>文章</a>
                    <a class="item" href="./product_list.html"><i class="cloud icon"></i>用户</a>
                    <a class="item" href="./user_profile.html"><i class="info icon"></i>类别</a>
                </div>
            </div>
        </div>
			<#nested>
			<!-- END PAGE CONTAINER-->
    </div>
    <!--footer begin-->
    <footer>
        <div id="copyrights">
            <div class="inset">
                <div class="bigcontainer">
                    <div class="fl">
                        <div class="logo"></div>
                        <p>&copy; 2020 热橙信息科技（上海）有限公司, 版权所有<br />
                            <a href="http://www.miibeian.gov.cn" target="_blank">鲁ICP备12022271号</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

</#macro>
<#macro scripts>
    <script type="text/javascript" src="${CONTEXT_PATH}/resource/javascript/jquery.min.js"></script>
    <script type="text/javascript" src="${CONTEXT_PATH}/resource/javascript/framework.js"></script>
    <script>
        $(document).ready(function(){
            $('.ui.dropdown')
                    .dropdown()
            ;
        });
    </script>
	  <#nested>
</body>
<!-- END BODY -->
</html>
</#macro>